include .env

# Executables (local)
DOCKER_COMP=$(shell echo $(DOCKER_COMPOSE))

# Docker containers
PHP_CONT = $(DOCKER_COMP) exec -u astradock php-apache
POSTGRES_CONT = $(DOCKER_COMP) exec -u postgres postgres

# Executables
PHP      = $(PHP_CONT) php

# Misc
.DEFAULT_GOAL = help
.PHONY        = help build up start ps down restart logs bash psql db

## —— 🎵 🐳 The Astradock Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) build --pull --no-cache

up: ## Start the Astradock hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

init: down build up ## Build and start the containers

ps: ## Show running containers
	@$(DOCKER_COMP) ps

stop: ## Stop all containers
	@$(DOCKER_COMP) stop

down: ## Stops containers and removes containers, networks, volumes, and images created by up.
	@$(DOCKER_COMP) down --remove-orphans

restart: stop up ## Restart containers

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

bash: ## Connect to the php-apache container
	@$(PHP_CONT) bash

psql: ## Connect to the postgres container
	@$(POSTGRES_CONT) psql

project: ## Create new virtual host
	@$(PHP_CONT) /etc/apache2/create-project.sh
	@$(DOCKER_COMP) restart php-apache

## —— Postgres 🐘 ——————————————————————————————————————————————————————————————
db: ## Run psql create database, pass the parameter "n=" name of database, example: make db n='database_name'
	@$(eval n ?=)
	@$(POSTGRES_CONT) psql -U postgres -c "CREATE DATABASE $(n)"

# —— Develop image 💻 ————————————————————————————————————————————————————————————————
build-images: ## Build images
	DOCKER_BUILDKIT=1 docker --log-level=debug build --pull \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
    	--cache-from ${REGISTRY}/php-apache:cache \
    	--tag ${REGISTRY}/php-apache:cache \
    	--tag ${REGISTRY}/php-apache:${IMAGE_TAG} \
    	--file php-apache/Dockerfile php-apache

try-build-images: ## Try build images with localhost registry
	REGISTRY=localhost IMAGE_TAG=0 make build-images

push-images-cache: ## Push images in registry with cache tag
	docker push ${REGISTRY}/php-apache:cache

push-images: ## Push images in registry
	docker push ${REGISTRY}/php-apache:${IMAGE_TAG}
