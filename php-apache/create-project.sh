#!/bin/bash

set -e

read -p "Введите название проекта (например - secretary): " project_name

file_name=$project_name.conf

cd /etc/apache2/sites-available
cp -p host.conf.example $file_name
sed -i "s/project_name/$project_name/g" $file_name

echo $project_name >> /var/www/projects.list

echo "Виртуальный хост создан! Ваш проект будет доступен по адресу http://$project_name.localhost"
